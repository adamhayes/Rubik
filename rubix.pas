 program rubix;
 uses crt, sysutils;


           {          Solves Rubik's Cube         }
           {          (WITHOUT  CHEATING)         }
           {                  by                  }
           {                                      }
           {                                      }
           {                  A                   }
           {                  D                   }
           {        KENNETH BRANNIGAN             }
           {                  M                   }
           {                                      }
           {                  H                   }
           {                  A                   }
           {                  Y                   }
           {                  E                   }
           {                  S                   }
type
   rubixarray=array[1..3,1..3,1..6] of integer;
   st=(cl,cr,mc,mr);
   sarray=array[1..12] of integer;
   rarray=array[1..216] of integer;
   tarray=array[1..1000,1..3] of integer;
   farray=array[1..9,1..3] of integer;






const
   rotarray: array[1..216] of integer =
   (3,3,3,1,1,1,3,3,3,3,3,3,
    3,2,1,1,2,3,3,2,1,3,2,1,
    5,5,5,2,2,2,6,6,6,4,4,4,
    3,2,1,1,1,1,1,2,3,3,3,3,
    1,1,1,1,2,3,3,3,3,3,2,1,
    5,5,5,3,3,3,6,6,6,1,1,1,
    1,1,1,1,1,1,1,1,1,3,3,3,
    1,2,3,1,2,3,1,2,3,3,2,1,
    5,5,5,4,4,4,6,6,6,2,2,2,
    1,2,3,1,1,1,3,2,1,3,3,3,
    3,3,3,1,2,3,1,1,1,3,2,1,
    5,5,5,1,1,1,6,6,6,3,3,3,
    3,2,1,3,2,1,3,2,1,3,2,1,
    1,1,1,1,1,1,1,1,1,1,1,1,
    2,2,2,1,1,1,4,4,4,3,3,3,
    1,2,3,1,2,3,1,2,3,1,2,3,
    3,3,3,3,3,3,3,3,3,3,3,3,
    4,4,4,1,1,1,2,2,2,3,3,3);

   midarray: array[1..108] of integer =
      (2,2,2,2,2,2,2,2,2,2,2,2,
       1,2,3,3,2,1,3,2,1,3,2,1,
       2,2,2,6,6,6,4,4,4,5,5,5,
       2,2,2,1,2,3,2,2,2,3,2,1,
       1,2,3,2,2,2,3,2,1,2,2,2,
       3,3,3,6,6,6,1,1,1,5,5,5,
       1,2,3,1,2,3,1,2,3,1,2,3,
       2,2,2,2,2,2,2,2,2,2,2,2,
       1,1,1,2,2,2,3,3,3,4,4,4);
    {rotate middle parallel with 1,2,6}

   edgearray: array[1..72] of integer =
      (3,2,5,1,2,2,3,2,6,3,2,4,
       2,1,5,1,2,3,2,3,6,3,2,1,
       1,2,5,1,2,4,1,2,6,3,2,2,
       2,3,5,1,2,1,2,1,6,3,2,3,
       2,1,2,2,1,1,2,1,4,2,1,3,
       2,3,4,2,3,1,2,3,2,2,3,3);
       {coordinates that match with edge pieces starting with side 2,1,1}

   cornerarray: array[1..144] of integer =
      (3,1,4,3,3,5,3,1,5,1,1,2,1,3,2,3,3,6,3,1,6,3,3,4,
       3,1,1,3,1,5,1,1,5,1,1,3,1,3,3,1,3,6,3,3,6,3,3,1,
       3,1,2,1,1,5,1,3,5,1,1,4,1,3,4,1,1,6,1,3,6,3,3,2,
       3,1,3,1,3,5,3,3,5,1,1,1,1,3,1,3,1,6,1,1,6,3,3,3,
       1,1,3,3,1,2,1,1,2,3,1,1,1,1,1,3,1,4,1,1,4,3,1,3,
       3,3,3,1,3,4,3,3,4,1,3,1,3,3,1,1,3,2,3,3,2,1,3,3);
       {coordinates for corners starting with left side of 1,1,1}
   movarray: array[1..54] of integer =
       (1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,3,3,3,4,4,4,5,5,5,
        1,2,1,1,2,1,1,2,1,1,2,1,1,2,1,1,2,1,1,2,1,1,2,1,1,2,1);
   movearray: array[1..27] of st =
       (cl,cl,cr,cl,cl,cr,cl,cl,cr,cl,cl,cr,cl,cl,cr,cl,cl,cr,mc,mc,mr,mc,mc,
       mr,mc,mc,mr);



   textfile='datafile';


var
   mainarray,temparray:rubixarray;
   col,stack1, stack2:sarray;
   nummove,time,slow,x,y,m,n,mm,nn,oo,putz:integer;
   fname,outfile:text;
   choice,save,z,food:char;
   backmove,ddir:st;
   moves:tarray;
   findarray:farray;
   done,color:boolean;
   p:string[2];
   mlist:rarray;
   printmove:boolean=true;




procedure loadarrays;
var
   a,b,c:integer;
begin
   {clrscr;}
   for c:=1 to 6 do
      begin
         for b:=1 to 3 do
            begin
               for a:=1 to 3 do
                  begin
                     mainarray[a,b,c]:=c;
                  end;
            end;
      end;
end;

function colortosidenumber(var letter:char) : integer;
{Convert a color letter to a color number.}

var
        number:integer;

begin
        if letter='b' then colortosidenumber:= 1;
        if letter='o' then colortosidenumber:= 2;
        if letter='g' then colortosidenumber:= 3;
        if letter='r' then colortosidenumber:= 4;
        if letter='w' then colortosidenumber:= 5;
        if letter='y' then colortosidenumber:= 6;
end;

function colortocolornumber(var letter:char) : integer;
{Convert a color letter to a color number.}

var
        number:integer;

begin
        if letter='b' then colortocolornumber:= 1;
        if letter='o' then colortocolornumber:= 92;
        if letter='g' then colortocolornumber:= 2;
        if letter='r' then colortocolornumber:= 4;
        if letter='w' then colortocolornumber:= 15;
        if letter='y' then colortocolornumber:= 14;
end;

procedure enterboard;
var
    d,e,f,g,h,i,j:integer;
    square:string[1];
    colorletter:char;

begin
   clrscr;
   writeln('Enter r,g,b,w,o,y.');
   for d:=1 to 2 do
      begin
         for e:=1 to 3 do
            begin
               for f:=1 to 3 do
                  begin
                     gotoxy(55+f,8*d+e);
                     readln(colorletter);
                     mainarray[f,e,4+d]:=colortocolornumber(colorletter);
                  end;
            end;
      end;
   j:=23;
   for g:=1 to 4 do
      begin
         for h:=1 to 3 do
            begin
               for i:=1 to 3 do
                  begin
                     gotoxy(20+j+i,12+h);
                     readln(colorletter);
                     mainarray[i,h,g]:=colortocolornumber(colorletter);
                  end;
            end;
         j:=j+4;
      end;
end;


procedure drawboard;
var
    d,e,f,g,h,i,j:integer;
    square:string[1];
    colorremap: array[1..6] of char = ('b','o','g','r','w','y');

begin
square:='X'; {chr(219);}
   for d:=1 to 2 do
      begin
         for e:=1 to 3 do
            begin
               for f:=1 to 3 do
                  begin
                     textcolor(colortocolornumber(colorremap[mainarray[f,e,4+d]]));
                     gotoxy(55+f,8*d+e);
                     if not color then str(mainarray[f,e,4+d],square);
                     write(square);
                  end;
            end;
      end;
   j:=23;
   for g:=1 to 4 do
      begin
         for h:=1 to 3 do
            begin
               for i:=1 to 3 do
                  begin
                     textcolor(colortocolornumber(colorremap[mainarray[i,h,g]]));
                     gotoxy(20+j+i,12+h);
                     if not color then str(mainarray[i,h,g],square);
                     write(square);
                  end;
            end;
         j:=j+4;
      end;
   delay(time);
   writeln();
   writeln();
   writeln();
   writeln();
   writeln();
   writeln();
   writeln();
   writeln();
end;


procedure rotate(axis,rep:integer; dir:st);

   {cl=clockwise}
   {cr=counter clockwise}
   {mc=middle clockwise}
   {mr=middle counter clockwise}

var a,b,c,d,loop,side:integer;
    directionstring:string;
    tempchar:char;

begin

 case dir of
    cl:begin
         d:=1;
         directionstring:='CW         ';
       end;
    cr:begin
         d:=2;
         directionstring:='C-CW       ';
       end;
    mc:begin
         d:=3;
         directionstring:='middle CW  ';
       end;
    mr:begin
         d:=4;
         directionstring:='middle C-CW';
       end;
 end;
   if save='Y' then
      begin
         { Write to screen. }
         writeln(fname,axis);
         writeln(fname,rep);
         writeln(fname,d);
      end;
      if printmove=true then
         begin
              gotoxy(5,25);
              textcolor(15);
              writeln('Side: ',axis,' Quarter-turns: ',rep,' Dir: ',directionstring,' <enter>');
              read(tempchar);
              if tempchar='s' then printmove:=false;
         end;

if ((dir=cl) or (dir=cr)) then
begin
   for loop:=1 to rep do
   begin
             c:=1;

               for b:=1 to 3 do
               begin
                   for a:=1 to 3 do
                       begin
                       stack1[c]:=mainarray[a,b,axis];
                       c:=c+1;
                       end
               end;
   case dir of
    cl:begin
          c:=1;
               for b:=3 downto 1 do
               begin
                    for a:=1 to 3 do
                    begin
                    mainarray[b,a,axis]:=stack1[c];
                    c:=c+1;
                    end
               end
    end;{case selector 'cl'}
    cr :begin
              c:=1;
              for b:=1 to 3 do
              begin
                   for a:=3 downto 1 do
                   begin
                   mainarray[b,a,axis]:=stack1[c];
                   c:=c+1;
                   end

              end
     end{case selector'cr'}

   end{case}
end;{for 'loop' loop}
    for a:=1 to 12 do
    stack1[a]:=mainarray[rotarray[36*(axis-1)+a],rotarray[36*(axis-1)+12+a],
               rotarray[36*(axis-1)+24+a]];
    case dir of
    cl:begin
              for a:=13-(rep*3) to 12 do
                  stack2[a-rep*3]:=stack1[a];
              for a:=12-(rep*3) downto 1 do
                  stack1[a+rep*3]:=stack1[a];
              for a:=1 to rep*3 do
                  stack1[a]:=stack2[a+12-rep*3-rep*3];
         end;{case selector cl}
    cr:begin
              for a:=1 to rep*3 do
                  stack2[a+12-rep*3]:=stack1[a];
              for a:=rep*3+1 to 12 do
                  stack1[a-rep*3]:=stack1[a];
              for a:=13-rep*3 to 12 do
                  stack1[a]:=stack2[a];
         end;{case selector cr}
    end;{case}
for a:=1 to 12 do
    mainarray[rotarray[36*(axis-1)+a],rotarray[36*(axis-1)+12+a], rotarray[36*(axis-1)+24+a]]:=stack1[a];
end
else
   begin
   side:=axis;
   case axis of
   3:begin
     side:=1;
     if dir=mc then dir:=mr
     else
     dir :=mc;
     end;

   4:begin
      side:=2;
      if dir=mc then dir:=mr
      else
      dir:=mc;
      end;
   5:begin
      side:=3;
      if dir=mc then dir:=mr
      else
      dir:=mc;
      end;
   6:begin
         side:=3;
      end;
 end;
   for a:=1 to 12 do
     stack1[a]:=mainarray[midarray[36*(side-1)+a],midarray[36*(side-1)+12+a],
                midarray[36*(side-1)+24+a]];
   case dir of
         mc:begin
              for a:=13-(rep*3) to 12 do
                  stack2[a-rep*3]:=stack1[a];
              for a:=12-(rep*3) downto 1 do
                  stack1[a+rep*3]:=stack1[a];
              for a:=1 to rep*3 do
                  stack1[a]:=stack2[a+12-rep*3-rep*3];
            end;
         mr:begin
              for a:=1 to rep*3 do
                  stack2[a+12-rep*3]:=stack1[a];
              for a:=rep*3+1 to 12 do
                  stack1[a-rep*3]:=stack1[a];
              for a:=13-rep*3 to 12 do
                  stack1[a]:=stack2[a];
            end;
      end;
    for a:=1 to 12 do
        mainarray[midarray[36*(side-1)+a],midarray[36*(side-1)+12+a],
                midarray[36*(side-1)+24+a]]:=stack1[a];

   end;
  if (food='y')or (food='Y') then
    drawboard;
  (*
   *if (printmove=true) then
   *  begin
   *    readln();
   *  end;
   *)
end;{procedure rotarray}

procedure rmove;

var
  a,b,c,d,e:integer;

begin
    clrscr;
    write('How many moves? ');
    readln(a);
    clrscr;
    drawboard;
    if printmove=true then writeln('  ''s'' to stop prompting');
    {Seed the random number generator once.}
    randomize;
    for b:=1 to a do
      begin
        c:=random(6)+1;
        d:=random(2)+1;
        e:=random(4)+1;
        case e of
           1:ddir:=cl;
           2:ddir:=cr;
           3:ddir:=mc;
           4:ddir:=mr;
         end;{case}
        rotate(c,d,ddir);
        drawboard;
        writeln('');
        writeln('  ',b);
      end;

end;


procedure playback;
   var
   a,b:integer;

   begin
      reset(fname);
      a:=0;
      repeat
         a:=a+1;
         readln(fname,moves[a,1]);
         readln(fname,moves[a,2]);
         readln(fname,moves[a,3]);
      until eof(fname);
      for b:=a downto 1 do
         begin
            case moves[b,3] of
               1:ddir:=cr;
               2:ddir:=cl;  {This part reverses dir of each move}
               3:ddir:=mr;
               4:ddir:=mc;
            end;
            rotate(moves[b,1],moves[b,2],ddir);
            write(b);
         end;
   close(fname);
   end;{end procedure}


procedure savedia;

var
  a,b,c:integer;

begin
  assign (fname,'datafile.dia');
  rewrite(fname);
  for a:=1 to 6 do
    begin
      for b:=1 to 3 do
        begin
          for c:=1 to 3 do
            writeln(fname,mainarray[c,b,a]);
        end;
    end;
  close(fname);
  assign(fname,'datafile');
end;{procedure}


procedure readboard;

var
 a,b,c:integer;
 colorchar:char;

begin
  assign(fname,'cubecolorpositions.txt');
  reset(fname);
  for a:=1 to 6 do
    begin
      for b:=1 to 3 do
        begin
          for c:=1 to 3 do
            begin
            {readln(fname,mainarray[c,b,a]);}
            readln(fname,colorchar);
            {write(colorchar,' ',colortosidenumber(colorchar));}
            {write(colorchar);}
            mainarray[c,b,a]:=colortosidenumber(colorchar);
            end;
            {writeln()}
        end;
    end;
  close(fname);
  assign(fname,'datafile');
  {writeln('enter to return to menu');}
  {readln();}
end;{procedure}

procedure loaddia;

var
 a,b,c:integer;

begin
  assign(fname,'datafile.dia');
  reset(fname);
  for a:=1 to 6 do
    begin
      for b:=1 to 3 do
        begin
          for c:=1 to 3 do
            readln(fname,mainarray[c,b,a]);
        end;
    end;
  close(fname);
  assign(fname,'datafile');
end;{procedure}


procedure convert;

var
a,b,c:integer;
d,e,f:string[22];
h:string[1];
g:string[71];

begin
     reset(fname);
     assign(outfile,'solution.txt');
     rewrite(outfile);
     repeat
                readln(fname,a);
                readln(fname,b);
                readln(fname,c);
                d:=' quarter turns ';
                case c of
                     1,3:e:=' clockwise.';
                     2,4:e:=' counter-clockwise.';
                end;
                case c of
                     1,2:f:='Turn           face   ';
                     3,4:f:='Turn center of side   ';
                end;
                str(a,h);
                g:=concat(f,h,'   ',inttostr(b),d,e);
                writeln(outfile,g);
     until eof(fname);
     close(fname);
     close(outfile);
     end;{procedure}




procedure find(tc:integer);

var
a,b,c,d:integer;

begin
     d:=1;
     for a:=1 to 6 do
       begin
         for b:=1 to 3 do
           begin
             for c:=1 to 3 do
               begin
               if (mainarray[c,b,a]=tc) then
                 begin
                 findarray[d,1]:=c;
                 findarray[d,2]:=b;
                 findarray[d,3]:=a;
                 d:=d+1;
                 end
               end
           end
       end;
end;

procedure cornmatch(i,n:integer);
var
   o,a,x,y:integer;
   flag:boolean;

begin
   a:=0;
   repeat
      a:=a+1;
      x:=1;
      y:=1;
      if ((findarray[a,1]+findarray[a,2])/2=int((findarray[a,1]+findarray[a,2]
         )/2)) and (findarray[a,1]<>2) then
         begin
            o:=findarray[a,1]*2+findarray[a,2];
            case o of
               3:o:=0;
               7:o:=6;
               9:o:=12;
               5:o:=18;
            end;
            x:=(findarray[a,3]-1)*24+1+o;
            y:=x+3;
            flag:=true;
         end
      else
         flag:=false;
   until ((mainarray[cornerarray[x],cornerarray[x+1],cornerarray[x+2]]=col[i])
   or (mainarray[cornerarray[x],cornerarray[x+1],cornerarray[x+2]]=col[n]))
   and ((mainarray[cornerarray[y],cornerarray[y+1],cornerarray[y+2]]=col[i])
   or (mainarray[cornerarray[y],cornerarray[y+1],cornerarray[y+2]]=col[n]))and(flag);
         mm:=a;
         nn:=x;
         oo:=y;
end;{end of procedure}

procedure match(s:integer);
var
   b,f,c,d,o:integer;
   flag:boolean;
begin
     b:=0; f:=1;
     repeat
       b:=b+1;
       c:=findarray[b,1]*2+findarray[b,2];
       d:=findarray[b,1]+findarray[b,2];
       if int(d/2)<>d/2 then
         begin
           case c of
             5:o:=1;
             8:o:=4;
             7:o:=7;
             4:o:=10;
           end;{case}
           f:=(findarray[b,3]-1)*12+o;
           flag:=true;
         end
         else
           flag:=false;
     until (mainarray[edgearray[f],edgearray[f+1],edgearray[f+2]]=col[s])
           and (flag);
     m:=b;
     n:=f;

end;

procedure check;
var
   a,b,c:integer;
   quitflag:boolean = false;

begin
   for a:=1 to 6 do
      begin
         for b:=1 to 3 do
            begin
               for c:=1 to 3 do
                  begin
                     if (mainarray[c,b,a]=mainarray[2,2,a]) then
                        done:=true
                     else
                        begin
                           {a:=6;}
                           {b:=3;}
                           {c:=3;}
                           done:=false;
                           quitflag:=true;
                           break;
                        end;
                  end; {loop c}
            if quitflag then
                begin
                    break;
                end;
            end; {loop b}
      if quitflag then
          begin
              break;
          end;
      end; {loop a}
end;


procedure edge;

var a,b,c,d,g,h:integer;

begin
for a:=1 to 4 do
  begin
   if a=1 then b:=4 else b:=a-1;
   if a=4 then c:=1 else c:=a+1;
   if b=1 then d:=4 else d:=b-1;
   if (mainarray[1,2,a]<>col[a]) or (mainarray[3,2,b]<>col[b]) then
     begin
       find(col[a]);
       match(b);
       if (findarray[m,3]<5) and (edgearray[n+2]<5) then
           begin
               if (findarray[m,1]=1) and (findarray[m,2]=2) then g:=findarray[m,3]
               else g:=edgearray[n+2];
               rotate(g,1,cr);
               rotate(6,2,cr);
               rotate(g,1,cl);
               rotate(6,2,cr);
               if g=1 then h:=4
                  else h:=g-1;
               rotate(h,1,cl);
               rotate(6,1,cl);
               rotate(h,1,cr);
           end;
       find(col[a]);
       match(b);
       if findarray[m,3]<6 then
          begin
            repeat
              rotate(6,1,cl);
              find(col[a]);
              match(b);
            until findarray[m,3]=c;
            rotate(b,1,cl);
            rotate(6,1,cr);
            rotate(b,1,cr);
            rotate(6,1,cr);
            rotate(a,1,cr);
            rotate(6,1,cl);
            rotate(a,1,cl);
          end
        else
          begin
            repeat
              rotate(6,1,cl);
              find(col[a]);
              match(b);
            until edgearray[n+2]=d;
            rotate(a,1,cr);
            rotate(6,1,cl);
            rotate(a,1,cl);
            rotate(6,1,cl);
            rotate(b,1,cl);
            rotate(6,1,cr);
            rotate(b,1,cr);
          end;{if}
        end;{if unsolved}
      end;{for}
    end;{procedure}

procedure doone(side:integer);
var
   ab,this,that,there:integer;
begin
   this:=side-1;
   if this=0 then this:=4;
   that:=this-1;
   if that=0 then that:=4;
   there:=that-1;
   if there=0 then there:=4;
   rotate(this,1,mc);
   rotate(6,2,cl);
   rotate(this,1,mr);
   rotate(side,1,cr);
   rotate(5,2,mc);
   rotate(side,2,cl);
   rotate(5,2,mr);
   rotate(side,1,cr);
   rotate(6,2,cr);
   if (mainarray[2,3,there]<>col[6]) then
      begin
         rotate(6,1,cl);
         rotate(there,1,cl);
         rotate(6,1,cr);
         rotate(there,1,cr);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(6,1,cl);
         rotate(that,1,cr);
         rotate(5,1,cl);
         rotate(6,2,cl);
      end
   else
      begin
         rotate(that,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(this,1,cr);
         rotate(6,1,cr);
         rotate(this,1,cl);
         rotate(5,1,cl);
         rotate(6,1,cr);
      end;
   find(col[that]);
   match(6);
   if(findarray[m,3]=6) then
      begin
         ab:=there-edgearray[n+2];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(that,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(this,1,cr);
         rotate(6,1,cr);
         rotate(this,1,cl);
         rotate(5,1,cl);
         rotate(6,2,cl);
      end
   else
      begin
         ab:=that-findarray[m,3];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(there,1,cl);
         rotate(6,1,cr);
         rotate(there,1,cr);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(6,1,cl);
         rotate(that,1,cr);
         rotate(5,1,cl);
         rotate(6,1,cl);
      end;
   find(col[that]);
   match(this);
   if (findarray[m,3]<>6) then
      begin
         ab:=that-findarray[m,3];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(there,1,cl);
         rotate(6,1,cr);
         rotate(there,1,cr);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(6,1,cl);
         rotate(that,1,cr);
         rotate(5,1,cl);
         rotate(6,1,cl);
      end
   else
      begin
         ab:=there-edgearray[n+2];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(that,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(this,1,cr);
         rotate(6,1,cr);
         rotate(this,1,cl);
         rotate(5,1,cl);
         rotate(6,2,cr);
      end;
   find(col[that]);
   match(there);
   if (findarray[m,3]<>6) then
      begin
         ab:=there-findarray[m,3];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(that,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(this,1,cr);
         rotate(6,1,cr);
         rotate(this,1,cl);
         rotate(5,1,cl);
         rotate(6,1,cr);
      end
   else
      begin
         ab:=that-edgearray[n+2];
         if (ab<0)and(ab<>-3) then
            begin
               ab:=abs(ab);
               rotate(6,ab,cr);
            end
         else if (ab=-3) then
            rotate(6,1,cl)
         else if (ab=3) then
            rotate(6,1,cr)
         else
            rotate(6,ab,cl);
         rotate(there,1,cl);
         rotate(6,1,cr);
         rotate(there,1,cr);
         rotate(5,1,cr);
         rotate(6,1,cl);
         rotate(that,1,cl);
         rotate(6,1,cl);
         rotate(that,1,cr);
         rotate(5,1,cl);
         rotate(6,2,cl);
      end;

end;





procedure finish(side:integer);
var
   other,other2,other3:integer;
begin
   other:=side+1;
   if other=5 then other:=1;
   other2:=other+1;
   if other2=5 then other2:=1;
   other3:=other2+1;
   if other3=5 then other3:=1;
   rotate(side,1,mr);
   if (mainarray[2,3,other2]=col[other2]) then
    begin
      rotate(6,1,cl);
      rotate(side,1,mc);
      rotate(6,1,cr);
    end
   else
    begin
      rotate(6,1,cr);
      rotate(side,1,mc);
      rotate(6,1,cl);
    end;
   rotate(other3,1,cr);
   rotate(5,1,mc);
   rotate(other3,1,cl);
   rotate(5,1,mr);
   rotate(6,2,cl);
   rotate(side,1,cl);
   rotate(6,1,cr);
   rotate(side,1,cr);
   rotate(5,1,cl);
   rotate(6,1,cr);
   rotate(other3,1,cl);
   rotate(6,1,cl);
   rotate(other3,1,cr);
   rotate(5,1,cr);
   rotate(6,1,cr);
   if (mainarray[2,3,side]=col[other3]) then
    begin
     rotate(other3,1,cl);
     rotate(6,1,cr);
     rotate(other3,1,cr);
     rotate(5,1,cl);
     rotate(6,1,cr);
     rotate(other2,1,cl);
     rotate(6,1,cl);
     rotate(other2,1,cr);
     rotate(5,1,cr);
     rotate(6,1,cl);
     rotate(other2,1,cl);
     rotate(6,1,cr);
     rotate(other2,1,cr);
     rotate(5,1,cr);
     rotate(6,1,cl);
     rotate(other3,1,cl);
     rotate(6,1,cl);
     rotate(other3,1,cr);
     rotate(5,1,cl);
     rotate(6,1,cr);
    end
   else
    begin
     rotate(6,1,cr);
     rotate(side,1,cr);
     rotate(6,1,cl);
     rotate(side,1,cl);
     rotate(5,1,cl);
     rotate(6,1,cr);
     rotate(other3,1,cr);
     rotate(6,1,cr);
     rotate(other3,1,cl);
     rotate(5,1,cr);
     rotate(6,2,cl);
     rotate(other2,1,cl);
     rotate(6,1,cr);
     rotate(other2,1,cr);
     rotate(5,1,cr);
     rotate(6,1,cl);
     rotate(other3,1,cl);
     rotate(6,1,cl);
     rotate(other3,1,cr);
     rotate(5,1,cl);
     rotate(6,1,cr);
     end;
end;




procedure solve;

label 10;
label 11;
var
   a,b,c,d,e,o,g,h,aa,i,j,dork,clatch,dlatch,side,count:integer;
   corner1,corner2,corner3,corner4,unsolved,flag,diag1,diag2:boolean;

begin

   if printmove=true then writeln('  ''s'' to stop prompting');
{solve top side edge pieces}
   for a:=1 to 6 do
     col[a]:=mainarray[2,2,a];

   for a:=1 to 4 do
   begin
     unsolved:=true;
     find(col[5]);
     match(a);
     aa:=(a-1)*12+1;
if ((mainarray[edgearray[aa],edgearray[aa+1],edgearray[aa+2]]<>col[5]) and
 (mainarray[edgearray[aa],edgearray[aa+1],edgearray[aa+2]]<>col[a])) or
 ((mainarray[2,1,a]<>col[5]) and (mainarray[2,1,a]<>col[a])) then
    begin
  10:if (findarray[m,3]=6) or ((findarray[m,3]<5) and (findarray[m,2]=3)) then
       {if piece is on bottom third}
       begin
            unsolved:=false;
            x:=a+1;
            if x=5 then x:=1;
            if findarray[m,3]<6 then
               begin
                    repeat
                    rotate(6,1,cl);    { rotation }
                    find(col[5]);
                    match(a);
                    until findarray[m,3]=x;
               end
            else
               begin
                    repeat
                    rotate(6,1,cl);    { rotation }
                    find(col[5]);
                    match(a);
                    until edgearray[n+2]=x;
               end;
           rotate(x,1,mr);    { rotation }
           rotate(6,1,cr);    { rotation }
           rotate(x,1,mc);    {now piece is placed, but may not be oriented}    { rotation }
       end;{if on bottom third}
    if (findarray[m,3]<5) and (findarray[m,1]<>2) then
    {if piece is on middle 3rd}
        begin
              unsolved:=false;
              x:=a-1;
              if x=0 then
                 x:=4;
              repeat
              rotate(6,1,mc);    { rotation }
              find(col[5]);
              match(a);
              until ((findarray[m,3]=x) and (findarray[m,1]=3)) or
                    ((edgearray[n+2]=x) and (edgearray[n]=3));
              rotate(6,1,mr);    { rotation }
              rotate(a,1,cr);    { rotation }
              rotate(6,1,mc);    { rotation }
              rotate(a,1,cl);    { rotation }
              repeat
                 rotate(6,1,mr);    { rotation }
                 find(col[5]);
                 match(a);
              until (mainarray[2,2,1]=col[1]);
        end;{if piece is on middle 3rd}

    if (findarray[m,3]=5) or ((findarray[m,3]<5) and (findarray[m,2]=1))
    and (unsolved) then
       begin{if piece is on top 3rd}

          unsolved:=false;
          if findarray[m,3]=5 then
             x:=edgearray[n+2]
          else
             x:=findarray[m,3];
          rotate(x,1,cl);    { rotation }
          find(col[5]);
          match(a);
          goto 10;
       end;{if piece is on top 3rd}


    end;{if piece not placed yet}
    {now check for proper orientation & correct}
    find(col[5]);
    match(a);
    if mainarray[2,1,a]<>col[a] then
       begin
          rotate(findarray[m,3],1,cl);    { rotation }
          rotate(6,1,mr);    { rotation }
          rotate(findarray[m,3],1,cl);    { rotation }
          rotate(6,1,mc);    { rotation }
       end;
 end;{for 1-4}
{Now solve top side corner pieces}
    for a:=1 to 4 do
       begin
          find(col[5]);
          c:=a-1;
          if c=0 then c:=4;
          cornmatch(a,c);
          if (findarray[mm,1]*100+findarray[mm,2]*10+findarray[mm,3]<>a+110)
          and (cornerarray[nn]*100+cornerarray[nn+1]*10+cornerarray[nn+2]<>a+110)
          and (cornerarray[oo]*100+cornerarray[oo+1]*10+cornerarray[oo+2]<>a+110)then
          begin
{if on top}    if (findarray[mm,3]=5) or (cornerarray[nn+2]=5) or (cornerarray[oo+2]
               =5) then
               begin
                   stack1[1]:=findarray[mm,3];
                   stack1[2]:=cornerarray[nn+2];
                   stack1[3]:=cornerarray[oo+2];
                   stack1[4]:=0;
                   stack1[5]:=5;
                   for b:=1 to 3 do
                      begin
                         if ((stack1[b]>stack1[4])and(stack1[b]<>5)) then
                            stack1[4]:=stack1[b];
                         if (stack1[b]<stack1[5]) then
                            stack1[5]:=stack1[b];
                      end;
                   if (stack1[4]=4)and(stack1[5]=1) then
                      stack1[4]:=1;
                   aa:=stack1[4];
                   rotate(aa,1,cr);    { rotation }
                   rotate(6,1,cr);    { rotation }
                   rotate(aa,1,cl);    { rotation }
               end;
               repeat
                  rotate(6,1,cl);    { rotation }
                  find(col[5]);
                  cornmatch(a,c);
              until  ((findarray[mm,1]*100+findarray[mm,2]*10+findarray[mm,3]=a+130)
              or (cornerarray[nn]*100+cornerarray[nn+1]*10+cornerarray[nn+2]=a+130)
              or (cornerarray[oo]*100+cornerarray[oo+1]*10+cornerarray[oo+2]=a+130));
               rotate(a,1,cr);    { rotation }
               rotate(6,1,cr);    { rotation }
               rotate(a,1,cl);    { rotation }
       end;
                          {reorient if necessary}
                     while mainarray[1,1,a]<>col[a] do
                      begin
                       rotate(a,1,cr);    { rotation }
                       rotate(6,1,cl);    { rotation }
                       rotate(a,1,cl);    { rotation }
                       rotate(6,1,cr);    { rotation }
                       rotate(a,1,cr);    { rotation }
                       rotate(6,1,cl);    { rotation }
                       rotate(a,1,cl);    { rotation }
                      end;

     end;
     edge;



      {now solve bottom corners}
     count:=0;
     repeat
        rotate(6,1,cl);    { rotation }
        diag1:=((mainarray[1,3,1]=col[1])or(mainarray[1,3,1]=col[6])or(mainarray[1,3,1]=col[4]))
              and((mainarray[3,3,4]=col[1])or(mainarray[3,3,4]=col[6])or(mainarray[3,3,4]=col[4]))
              and((mainarray[3,1,6]=col[1])or(mainarray[3,1,6]=col[6])or(mainarray[3,1,6]=col[4]));

       diag2:=((mainarray[1,3,3]=col[3])or(mainarray[1,3,3]=col[6])or(mainarray[1,3,3]=col[2]))
              and((mainarray[3,3,2]=col[3])or(mainarray[3,3,2]=col[6])or(mainarray[3,3,2]=col[2]))
              and((mainarray[1,3,6]=col[3])or(mainarray[1,3,6]=col[6])or(mainarray[1,3,6]=col[2]));
        count:=count+1;
     until (count=4)or((diag1)and(diag2));
     if (not(diag1)) or (not(diag2)) then
       begin
       repeat
        rotate(6,1,cl);    { rotation }
        corner1:=((mainarray[1,3,1]=col[1])or(mainarray[1,3,1]=col[6])or(mainarray[1,3,1]=col[4]))
              and((mainarray[3,3,4]=col[1])or(mainarray[3,3,4]=col[6])or(mainarray[3,3,4]=col[4]))
              and((mainarray[3,1,6]=col[1])or(mainarray[3,1,6]=col[6])or(mainarray[3,1,6]=col[4]));

        corner2:=((mainarray[1,3,2]=col[2])or(mainarray[1,3,2]=col[6])or(mainarray[1,3,2]=col[1]))
              and((mainarray[3,3,1]=col[2])or(mainarray[3,3,1]=col[6])or(mainarray[3,3,1]=col[1]))
              and((mainarray[3,3,6]=col[2])or(mainarray[3,3,6]=col[6])or(mainarray[3,3,6]=col[1]));

        corner3:=((mainarray[1,3,3]=col[3])or(mainarray[1,3,3]=col[6])or(mainarray[1,3,3]=col[2]))
              and((mainarray[3,3,2]=col[3])or(mainarray[3,3,2]=col[6])or(mainarray[3,3,2]=col[2]))
              and((mainarray[1,3,6]=col[3])or(mainarray[1,3,6]=col[6])or(mainarray[1,3,6]=col[2]));

        corner4:=((mainarray[1,3,4]=col[4])or(mainarray[1,3,4]=col[6])or(mainarray[1,3,4]=col[3]))
              and((mainarray[3,3,3]=col[4])or(mainarray[3,3,3]=col[6])or(mainarray[3,3,3]=col[3]))
              and((mainarray[1,1,6]=col[4])or(mainarray[1,1,6]=col[6])or(mainarray[1,1,6]=col[3]));
       until((corner1)and(corner2))or((corner2)and(corner3))or((corner3)and(corner4))or((corner4)and(corner1));
      if((corner1) and (corner2)) then
         side:=3;
      if((corner2) and (corner3)) then
         side:=4;
      if((corner3) and (corner4)) then
         side:=1;
      if((corner4) and (corner1)) then
         side:=2;
      dlatch:=side-1;
      if dlatch=0 then dlatch:=4;
      clatch:=dlatch-1;
      if clatch=0 then clatch:=4;
      rotate(side,1,cr);    { rotation }
      rotate(6,1,cr);    { rotation }
      rotate(side,1,cl);    { rotation }
      rotate(6,1,cl);    { rotation }
      rotate(dlatch,1,cl);    { rotation }
      rotate(6,1,cl);    { rotation }
      rotate(dlatch,1,cr);    { rotation }
      rotate(clatch,1,cl);    { rotation }
      rotate(6,1,cr);    { rotation }
      rotate(clatch,1,cr);    { rotation }
      rotate(5,1,cr);    { rotation }
      rotate(6,1,cl);    { rotation }
      rotate(dlatch,1,cl);    { rotation }
      rotate(6,1,cl);    { rotation }
      rotate(dlatch,1,cr);    { rotation }
      rotate(5,1,cl);    { rotation }
      rotate(6,1,cr);    { rotation }
      rotate(6,2,cr);    { rotation }
      rotate(side,1,cr);    { rotation }
      rotate(6,1,cl);    { rotation }
      rotate(side,1,cl);    { rotation }
      rotate(5,1,cl);    { rotation }
      rotate(6,1,cr);    { rotation }
      rotate(dlatch,1,cr);    { rotation }
      rotate(6,1,cr);    { rotation }
      rotate(dlatch,1,cl);    { rotation }
      rotate(5,1,cr);    { rotation }
      rotate(6,1,cr);    { rotation }
     repeat
        rotate(6,1,cl);    { rotation }
        corner1:=((mainarray[1,3,1]=col[1])or(mainarray[1,3,1]=col[6])or(mainarray[1,3,1]=col[4]))
              and((mainarray[3,3,4]=col[1])or(mainarray[3,3,4]=col[6])or(mainarray[3,3,4]=col[4]))
              and((mainarray[3,1,6]=col[1])or(mainarray[3,1,6]=col[6])or(mainarray[3,1,6]=col[4]));

        corner2:=((mainarray[1,3,2]=col[2])or(mainarray[1,3,2]=col[6])or(mainarray[1,3,2]=col[1]))
              and((mainarray[3,3,1]=col[2])or(mainarray[3,3,1]=col[6])or(mainarray[3,3,1]=col[1]))
              and((mainarray[3,3,6]=col[2])or(mainarray[3,3,6]=col[6])or(mainarray[3,3,6]=col[1]));

        corner3:=((mainarray[1,3,3]=col[3])or(mainarray[1,3,3]=col[6])or(mainarray[1,3,3]=col[2]))
              and((mainarray[3,3,2]=col[3])or(mainarray[3,3,2]=col[6])or(mainarray[3,3,2]=col[2]))
              and((mainarray[1,3,6]=col[3])or(mainarray[1,3,6]=col[6])or(mainarray[1,3,6]=col[2]));

        corner4:=((mainarray[1,3,4]=col[4])or(mainarray[1,3,4]=col[6])or(mainarray[1,3,4]=col[3]))
              and((mainarray[3,3,3]=col[4])or(mainarray[3,3,3]=col[6])or(mainarray[3,3,3]=col[3]))
              and((mainarray[1,1,6]=col[4])or(mainarray[1,1,6]=col[6])or(mainarray[1,1,6]=col[3]));
       until(corner1)and(corner2);
    end;{end of if}



  repeat
     if (mainarray[1,1,6]<>col[6]) or (mainarray[3,1,6]<>col[6])
     or (mainarray[1,3,6]<>col[6]) or (mainarray[3,3,6]<>col[6])
     or (mainarray[1,3,1]<>col[1]) or (mainarray[1,3,2]<>col[2])
     or (mainarray[1,3,3]<>col[3]) or (mainarray[1,3,4]<>col[4]) then
     begin
       repeat
              rotate(1,1,cl);    { rotation }
              rotate(4,1,cl);    { rotation }
              rotate(6,1,cr);    { rotation }
              rotate(4,1,cr);    { rotation }
              rotate(1,1,cr);    { rotation }
              rotate(2,1,cr);    { rotation }
              rotate(6,1,cl);    { rotation }
              rotate(2,1,cl);    { rotation }
              rotate(6,1,cl);    { rotation }
       until mainarray[1,3,1]=col[1];
     end;{if}
     if (mainarray[1,1,6]<>col[6]) or (mainarray[3,1,6]<>col[6])
     or (mainarray[1,3,6]<>col[6]) or (mainarray[3,3,6]<>col[6])
     or (mainarray[1,3,1]<>col[1]) or (mainarray[1,3,2]<>col[2])
     or (mainarray[1,3,3]<>col[3]) or (mainarray[1,3,4]<>col[4]) then
      begin
       repeat
            rotate(3,1,cl);    { rotation }
            rotate(2,1,cl);    { rotation }
            rotate(6,1,cr);    { rotation }
            rotate(2,1,cr);    { rotation }
            rotate(3,1,cr);    { rotation }
            rotate(4,1,cr);    { rotation }
            rotate(6,1,cl);    { rotation }
            rotate(4,1,cl);    { rotation }
            rotate(6,1,cl);    { rotation }
       until mainarray[1,3,3]=col[3];
     end;{if}
   until (mainarray[1,1,6]=col[6]) and(mainarray[3,1,6]=col[6])
     and (mainarray[1,3,6]=col[6]) and(mainarray[3,3,6]=col[6])
     and (mainarray[1,3,1]=col[1]) and(mainarray[1,3,2]=col[2])
     and (mainarray[1,3,3]=col[3]) and(mainarray[1,3,4]=col[4]);




{now solve bottom edges}
repeat
  repeat
   if (mainarray[2,3,1]=col[3])or(mainarray[3,2,6]=col[3]) then
      finish(2);
   if (mainarray[2,3,2]=col[4])or(mainarray[2,3,6]=col[4]) then
      finish(3);
   if (mainarray[2,3,3]=col[1])or(mainarray[1,2,6]=col[1]) then
      finish(4);
   if (mainarray[2,3,4]=col[2])or(mainarray[2,1,6]=col[2]) then
      finish(1);
   if (mainarray[2,3,1]=col[1]) and (mainarray[2,3,2]=col[2]) and (mainarray[2,3,3]=col[3])
   and (mainarray[2,3,4]=col[4]) then
    goto 11;
  until (mainarray[2,3,1]<>col[3])and(mainarray[3,2,6]<>col[3])and
    (mainarray[2,3,2]<>col[4])and(mainarray[2,3,6]<>col[4])and
    (mainarray[2,3,3]<>col[1])and(mainarray[1,2,6]<>col[1])and
    (mainarray[2,3,4]<>col[2])and(mainarray[2,1,6]<>col[2]);
  repeat
   if (mainarray[3,2,6]=col[1]) then
      doone(1)
   else if (mainarray[2,3,6]=col[2]) then
      doone(2)
   else if (mainarray[1,2,6]=col[3]) then
      doone(3)
   else if (mainarray[2,1,6]=col[4]) then
      doone(4);
   if (mainarray[2,3,1]=col[1]) and (mainarray[2,3,2]=col[2]) and (mainarray[2,3,3]=col[3])
   and (mainarray[2,3,4]=col[4]) then
    goto 11;
   if  (mainarray[2,3,1]<>col[3])and(mainarray[3,2,6]<>col[3])and
    (mainarray[2,3,2]<>col[4])and(mainarray[2,3,6]<>col[4])and
    (mainarray[2,3,3]<>col[1])and(mainarray[1,2,6]<>col[1])and
    (mainarray[2,3,4]<>col[2])and(mainarray[2,1,6]<>col[2])and
    (mainarray[3,2,6]<>col[1])and(mainarray[2,3,6]<>col[2])and
    (mainarray[1,2,6]<>col[3])and(mainarray[2,1,6]<>col[4])and
    ((mainarray[2,3,1]<>col[1])or(mainarray[2,3,2]<>col[2])or
    (mainarray[2,3,3]<>col[3])or(mainarray[2,3,4]<>col[4])) then
     finish(1);
  until (mainarray[2,3,1]=col[3])or(mainarray[3,2,6]=col[3])or
    (mainarray[2,3,2]=col[4])or(mainarray[2,3,6]=col[4])or
    (mainarray[2,3,3]=col[1])or(mainarray[1,2,6]=col[1])or
    (mainarray[2,3,4]=col[2])or(mainarray[2,1,6]=col[2]);
   if  (mainarray[2,3,1]<>col[3])and(mainarray[3,2,6]<>col[3])and
    (mainarray[2,3,2]<>col[4])and(mainarray[2,3,6]<>col[4])and
    (mainarray[2,3,3]<>col[1])and(mainarray[1,2,6]<>col[1])and
    (mainarray[2,3,4]<>col[2])and(mainarray[2,1,6]<>col[2])and
    (mainarray[3,2,6]<>col[1])and(mainarray[2,3,6]<>col[2])and
    (mainarray[1,2,6]<>col[3])and(mainarray[2,1,6]<>col[4])and
    ((mainarray[2,3,1]<>col[1])or(mainarray[2,3,2]<>col[2])or
    (mainarray[2,3,3]<>col[3])or(mainarray[2,3,4]<>col[4])) then
     finish(1);
until (mainarray[2,3,1]=col[1]) and (mainarray[2,3,2]=col[2]) and (mainarray[2,3,3]=col[3])
   and (mainarray[2,3,4]=col[4]);


11:writeln('   WOOT!');
read(choice);
end;{procedure}


procedure addmove(num:integer);

begin
nummove:=nummove+1;
mlist[nummove]:=num;
end;

procedure remove;

begin
     case movearray[mlist[nummove]] of
     cl:backmove:=cr;
     cr:backmove:=cl;
     mc:backmove:=mr;
     mr:backmove:=mc;
     end;{case}
     rotate(movarray[mlist[nummove]],movarray[mlist[nummove]+27],backmove);
     nummove:=nummove-1;
end;



procedure meister;

label 1;

var
a,b,c:integer;
possible:boolean;
x:string[3];

begin
nummove:=0;
possible:=true;
for c:=1 to 27 do
begin
     rotate(movarray[c],movarray[c+27],movearray[c]);
     check;
     addmove(c);
     if done then goto 1;
        for b:=1 to 27 do
            begin
                 rotate(movarray[b],movarray[b+27],movearray[b]);
                 addmove(b);
                 check;
                 if done then goto 1;
                    for a:=1 to 27 do
                        begin
                             rotate(movarray[a],movarray[a+27],movearray[a]);
                             addmove(a);
                             check;
                             if done then goto 1
                             else remove;
                        end;
                remove;
                end;
          remove;
    end;
    {if program gets here then it can't be solved in x moves}

possible:=false;
1:if possible then
       for a:=1 to nummove do
       begin
        writeln('');
        case movearray[mlist[a]] of
             cl:x:='cl';
             cr:x:='cr';
             mc:x:='mc';
             mr:x:='mr';
          end;{case}
        writeln(movarray[mlist[a]],movarray[mlist[a]+27],x);
       end;

writeln('WOOT');
(*
 *sound(1000);
 *delay(4000);
 *nosound;
 *)
readln(choice);
end;{procedure}


procedure minimize;
label 20;
label 40;
label 50;
label 30;
const
   filename='dude';
var
   da,db,dc,de,df,sally:integer;
   ez,fz,zz,yz,xz,uz,vz,dd,as,bs:integer;
   diskfile:text;
begin
   da:=0;
   db:=0;
   dc:=0;
   dd:=0;
   de:=0;
   df:=0;
  for de:= 1 to 27 do
     begin
         rotate(movarray[de],movarray[de+27],movearray[de]);
         check;
         if done then goto 20;
         for df:=1 to 27 do
            begin
               rotate(movarray[df],movarray[df+27],movearray[df]);
               check;
               if done then goto 20;
               for da:=1 to 27 do
                  begin
                     rotate(movarray[da],movarray[da+27],movearray[da]);
                     check;
                     if done then goto 20;
         for db:=1 to 27 do
            begin
               rotate(movarray[db],movarray[db+27],movearray[db]);
               check;
               if done then goto 20;
               for dc:=1 to 27 do
                  begin
                     rotate(movarray[dc],movarray[dc+27],movearray[dc]);
                     check;
                     if done then goto 20;
                     if movearray[dc]=cl then
                        movearray[dc]:=cr
                     else if movearray[dc]=cr then
                        movearray[dc]:=cl
                     else if movearray[dc]=mc then
                        movearray[dc]:=mr
                     else
                        movearray[dc]:=mc;
                     rotate(movarray[dc],movarray[dc+27],movearray[dc]);
                     dd:=dd+1;
                  end;
                     if movearray[db]=cl then
                        movearray[db]:=cr
                     else if movearray[db]=cr then
                        movearray[db]:=cl
                     else if movearray[db]=mc then
                        movearray[db]:=mr
                     else
                        movearray[db]:=mc;
                     rotate(movarray[db],movarray[db+27],movearray[db]);
                     dd:=dd+1;
                  end;
                      if movearray[da]=cl then
                        movearray[da]:=cr
                     else if movearray[da]=cr then
                        movearray[da]:=cl
                     else if movearray[da]=mc then
                        movearray[da]:=mr
                     else
                        movearray[da]:=mc;
                     rotate(movarray[da],movarray[da+27],movearray[da]);
                     dd:=dd+1;
                  end;
                     if movearray[df]=cl then
                        movearray[df]:=cr
                     else if movearray[df]=cr then
                        movearray[df]:=cl
                     else if movearray[df]=mc then
                        movearray[df]:=mr
                     else
                        movearray[df]:=mc;
                     rotate(movarray[df],movarray[df+27],movearray[df]);
                     dd:=dd+1;
                  end;
                      if movearray[de]=cl then
                        movearray[de]:=cr
                     else if movearray[de]=cr then
                        movearray[de]:=cl
                     else if movearray[de]=mc then
                        movearray[de]:=mr
                     else
                        movearray[da]:=mc;
                     rotate(movarray[da],movarray[da+27],movearray[da]);
                     dd:=dd+1;
                  end;
                write('not solvable in 3 moves');
                  goto 30;
                 20:food:='Y';
            rotate(movarray[de],movarray[de+27],movearray[de]);
            rotate(movarray[df],movarray[df+27],movearray[df]);
            rotate(movarray[da],movarray[da+27],movearray[da]);
            rotate(movarray[db],movarray[db+27],movearray[db]);
            rotate(movarray[dc],movarray[dc+27],movearray[dc]);
            writeln(movarray[de],movarray[de+27]);
            writeln(movarray[df],movarray[df+27]);
            writeln(movarray[da],movarray[da+27]);
            writeln(movarray[db],movarray[db+27]);
            writeln(movarray[dc],movarray[dc+27]);
       case movearray[de] of
          cl:uz:=1;
          cr:uz:=2;
          mc:uz:=3;
          mr:uz:=4;
         end;
       case movearray[df] of
          cl:vz:=1;
          cr:vz:=2;
          mc:vz:=3;
          mr:vz:=4;
         end;
       case movearray[da] of
          cl:zz:=1;
          cr:zz:=2;
          mc:zz:=3;
          mr:zz:=4;
         end;
        case movearray[db] of
          cl:xz:=1;
          cr:xz:=2;
          mc:xz:=3;
          mr:xz:=4;
         end;
       case movearray[dc] of
          cl:yz:=1;
          cr:yz:=2;
          mc:yz:=3;
          mr:yz:=4;
         end;
    assign(diskfile,'dude');
       rewrite(diskfile);
       write(diskfile,movarray[de]);
       write(diskfile,movarray[de+27]);
       writeln(diskfile,uz);
       write(diskfile,movarray[df]);
       write(diskfile,movarray[df+27]);
       writeln(diskfile,vz);
       write(diskfile,movarray[da]);
       write(diskfile,movarray[da+27]);
       writeln(diskfile,zz);
       write(diskfile,movarray[db]);
       write(diskfile,movarray[db+27]);
       writeln(diskfile,xz);
       write(diskfile,movarray[dc]);
       write(diskfile,movarray[dc+27]);
       writeln(diskfile,yz);
       close(diskfile);
       {whoop**********}
     30:for as:=1 to 3 do
        for bs:=2000 to 3000 do
        sound(bs);
        nosound;
 end;

var icolor:integer;
begin{MAIN PROGRAM}
   time:=0;
   color:=true;
   loadarrays;
   assign(fname,textfile);
   repeat
   drawboard;
   clrscr;
   textcolor(15);
   writeln('d]  Redraw');
   writeln('1]  Solve');
   writeln('2]  Store random moves');
   writeln('3]  Solve and store');
   writeln('4]  Play stored moves backwards');
   writeln('5]  Exit');
   writeln('6]  Randomize');
   writeln('7]  Toggle color on/off');
   writeln('8]  Custom Board');
   writeln('r]  Read Board in order sides 1...6');
   writeln('p]  Toggle prompting after each move');
   writeln('9]  rotate individual side');
   writeln('0]  Enter delay time');
   writeln('m]  Minimize moves');

   writeln;
   food:='y';
   readln(choice);
   drawboard;
   case choice of
     'd':begin
         write('    Press enter to return to menu.');
         drawboard;
         readln();
         end;
     'x':begin
          food:='n';
          meister;
         end;
     '1':begin
          save:='N';
          solve;
          end;
     '2':begin
          loadarrays;
          save:='Y';
          rewrite(fname);
          rmove;
          close(fname);
          savedia;
          end;
     '3':begin
          save:='Y';
          rewrite(fname);
          solve;
          close(fname);
          convert;
          end;
     '4':begin
          save:='N';
          loaddia;
          playback;
          end;
        '6':begin
           (*
            *slow:=time;
            *time:=0;
            *)
           save:='N';
           rmove;
           read(choice);
           (*
            *time:=slow;
            *)
           end;
     '7':if color then color:=false
         else color:=true;
     '8':enterboard;
     'p':begin
             { Toggle flag to prompt at each move. }
             if printmove=true then
               begin
                 printmove:=false;
                 writeln('Will NOT prompt after each move of the solution. Press Enter');
                 readln();
               end
             else
               begin
                 printmove:=true;
                 writeln('WILL prompt after each move of the solution. Press Enter');
                 readln();
               end;
         end;
     'r':begin
           readboard; {Read a board from a file.}
           drawboard;
           writeln('enter to return to menu');
           readln();
         end;
     '9':begin
           { Will call rotate(axis,rep:integer; dir:st)}
           {cl=clockwise}
           {cr=counter clockwise}
           {mc=middle clockwise}
           {mr=middle counter clockwise}
          repeat
            write('axis:  ');readln(x);
            if x=0 then
              begin
                writeln('DONE turning');
                break;
              end;
            write('turns: ');readln(y);
            write('dir:   ');readln(putz);
            case putz of
            1:rotate(x,y,cl);
            2:rotate(x,y,cr);
            3:rotate(x,y,mc);
            4:rotate(x,y,mr);
            end;
          until false;
         end;
     '0':begin
            clrscr;
            writeln('Please enter time delay: ');
            readln(time);
         end;
     'm':begin
            write('would you like to print to screen: ');
            readln(food);
            minimize;
         end;
     end;{case}
  until choice='5';
   clrscr;
   (*
    *for icolor:=1 to 256 do
    *begin
    *        textcolor(icolor);
    *        writeln(icolor);
    *end;
    *)
   halt

end.
