
RUBIX.PAS
==============

Solves Rubik's Cube.

Kenneth Brannigan and Adam Hayes

The original 1989 version of this code was compiled by Turbo Pascal on IBM XT.

This repository is a version updated for modern Pascal.  It compiles with
`fpc`.

High school senior project
--------------------------

The authors thought it would be amusing to put absolutely no comments in the
code and thus confound their teachers with the dilemma of failing them for
inexcusably unreadable code (and offensive variable names), or passing them so
that they did not have to put up with them for yet another year.

The code was tested on thousands of randomly-generated permutations of the Cube
and found to solve correctly.

It *looks like* `rubix.pas`, was the final version of the code.

![](pics/rubix2.png)

